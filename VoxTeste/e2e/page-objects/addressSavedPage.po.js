//addressSavedPage.po.js

var AddressSavedPage = function(){
    this.sucessAddressMessage = element(by.xpath(`//div[@class='alert alert-notice']`));
    this.firstNameRead = element(by.xpath(`//strong[contains(text(),'First name:')]`));
    this.lastNameRead = element(by.xpath(`//strong[contains(text(),'Last name:')]`));

    this.editButtonAddress = element(by.xpath(`//a[contains(text(),'Edit')]`));
    this.listButtonAddress = element(by.xpath(`//a[contains(text(),'List')]`));
    this.addressesMenuButton = element(by.xpath(`//a[contains(text(),'Addresses')]`));
};

AddressSavedPage.prototype.visit = function(){
    browser.get('addresses/')
};

module.exports = AddressSavedPage;