// newAddressPage.po.js

var NewAddressPage = function() {
    //this.tituloNewAddress = element(by.css('h2'));

    this.firstNameField = element(by.xpath(`//input[@id='address_first_name']`));
    this.lastNameField = element(by.id('address_last_name'));
    this.addressUmField = element(by.id('address_street_address'));
    this.addressDoisField = element(by.id('address_secondary_address'));
    this.cityField = element(by.id('address_city'));
    this.stateDropDown = element(by.id('address_state'));
    this.zipCodeField = element(by.id('address_zip_code'));
    this.countryRadioUS = element(by.id('address_country_us'));
    this.countryRadioCN = element(by.id('address_country_canada'));
    this.dateField = element(by.id('address_birthday'));
    this.colorInput = element(by.id('address_color'));
    this.ageField = element(by.xpath(`//input[@id='address_age']`));
    this.websiteField = element(by.xpath(`//input[@id='address_website']`));
    this.pictureFile = element(by.id('address_picture'));
    this.phoneField = element(by.xpath(`//input[@id='address_phone']`));
    this.commonCheckBoxClimbing = element(by.id('address_interest_climb'));
    this.commonCheckBoxDancing = element(by.id('address_interest_dance'));
    this.commonCheckBoxReading = element(by.id('address_interest_read'));
    this.noteField = element(by.id('address_note'));
    
    this.createAddressButton = element(by.xpath(`//input[@name='commit']`));
    this.listButton = element(by.xpath(`//a[@class='row justify-content-center']`));
};

NewAddressPage.prototype.visit = function() {
    browser.get('#/new');
};

module.exports = NewAddressPage;