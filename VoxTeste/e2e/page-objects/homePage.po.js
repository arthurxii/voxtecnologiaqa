//homePage.po.js

var HomePage = function(){
    this.tituloPaginaInicial = element(by.css('h1'));
    this.buttonHome = element(by.xpath(`//a[@class='nav-item nav-link active']`));
    this.buttonAdress = element(by.xpath(`//a[contains(text(),'Addresses')]`));
    this.buttonSignOut = element(by.xpath(`//a[contains(text(),'Sign out')]`));
    this.sucessMessage = element(by.xpath(`//h1[contains(text(),'Welcome to Address Book')]`));

};

HomePage.prototype.visit = function() {
//    browser.get('/http://a.testaddressbook.com/');
};

module.exports = HomePage;