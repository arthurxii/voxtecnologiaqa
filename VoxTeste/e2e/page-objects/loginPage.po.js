//loginPage.po.js

var LoginPage = function(){
    this.emailField = element(by.id('session_email'));
    this.passwordField = element(by.id('session_password'));
    this.submitButton = element(by.xpath(`//input[@name='commit']`));
    this.erroLogin = element(by.xpath(`//div[@class='alert alert-notice']`));

    this.sucessMessage = element(by.css('h1'));
};
    LoginPage.prototype.fillForm = function(email, password){
        this.emailField.sendKeys(email);
        this.passwordField.sendKeys(password);
    };

    LoginPage.prototype.visit = function(){
        browser.get('http://a.testaddressbook.com/sign_in');
    };

    module.exports = LoginPage;


