//addresseslistpage.spec.js
'use strict'
var AddresseslistPage = require('../addresseslistPage.po.js');

describe('addresseslistPage', function(){
    var addresseslistPage = new AddresseslistPage();

    it('clicar no botao show teste', function(){
        addresseslistPage.visit();

        addresseslistPage.showButton.click();

        expect(addressSavedPage.firstNameSave.isDisplayed()).toBe(true);
    });

    it('clicar no botao edit teste', function(){
        addresseslistPage.visit();

        addresseslistPage.addressesButton.click();
        addresseslistPage.editButton.click();

        expect(editAddressPage.tituloEditingAddress.isDisplayed()).toBe(true);
    });

});