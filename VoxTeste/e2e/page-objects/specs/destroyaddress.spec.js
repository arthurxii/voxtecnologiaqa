//destroyaddress.spec.js
'use strict'

var AddresseslistPage = require('../addresseslistPage.po.js');

describe('destroy address', function(){
    AddresseslistPage.visit();
    var addresseslistPage = AddresseslistPage();

  it('excluir cadastro teste', function(){
        addresseslistPage.visit();

        addresseslistPage.addressesButton.click();
        addresseslistPage.destroyButton.click();

        browser.manage().timeouts().implicitlyWait(30000);
    //    browser.get('http://a.testaddressbook.com/addresses');
        element(by.name("alert")).click();
        let confirmacao = browser.switchTo().alert();
        confirmacao.accept();
    });    
})