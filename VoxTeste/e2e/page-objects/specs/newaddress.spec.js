//newaddress.spec.js
'use strict'
var NewAddressPage = require('../newAddressPage.po.js');

describe('newAddressPage', function(){
    var newAddressPage = new NewAddressPage();

    it('Cadastrar endereco no sistema teste', function(){
    //    newAddressPage.visit();

       //browser.manage().timeouts().implicitlyWait(2000);

       //expect(newAddressPage.tituloNewAddress.isDisplayed().toBe(true));

        newAddressPage.firstNameField.sendKeys('Arthur');
        newAddressPage.lastNameField.sendKeys('Henrique');
        newAddressPage.addressUmField.sendKeys('Street Test');
        newAddressPage.addressDoisField.sendKeys('Test Two');
        newAddressPage.cityField.sendKeys('City of Test');
        newAddressPage.stateDropDown.click();
        //browser.sleep(1000);
        newAddressPage.stateDropDown.sendKeys('Arizona');
        newAddressPage.zipCodeField.sendKeys('58032843');
        newAddressPage.countryRadioUS.click();
        newAddressPage.dateField.sendKeys('04011995');
       // newAddressPage.colorInput.click();
        browser.sleep(1000);
       // newAddressPage.colorInput.sendKeys('#000000');
        newAddressPage.colorInput.submit();
        newAddressPage.ageField.sendKeys('24');
        newAddressPage.websiteField.sendKeys('http://websitetest.com');
 //       var path = require ('path');
 //       var dirname = 'C:/';
 //       var fileToUpload = '../teste.txt';
 //       var absolutePath = path.resolve('C:/Users/ARTHUR/Documents/teste.txt');

 //       newAddressPage.pictureFile.sendKeys(absolutePath);
 //       cb();

//        var path = require('path');
//        it('upload arquivo', function(){
//            var fileTopUpload = '../Users/ARTHUR/Documents/teste.txt',
//            absolutePath = path.resolve(__dirname, fileTopUpload);

//            newAddressPage.pictureFile.sendKeys(absolutePath);
//        });
        newAddressPage.phoneField.sendKeys('88995566');
        newAddressPage.commonCheckBoxDancing.click();
        newAddressPage.noteField.sendKeys('teste');
        
        newAddressPage.createAddressButton.submit();

   //     expect(AddressesPage.sucessfullyCreated.isDisplayed().toBe(true));
    });
});