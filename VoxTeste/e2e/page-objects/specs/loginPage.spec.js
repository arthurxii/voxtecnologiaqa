// loginPage.spec.js
'use strict'
var LoginPage = require('../loginPage.po.js');

describe('Loginpage', function(){
    var loginPage = new LoginPage();

    it('tentar logar na aplicacao sem informar os dados teste', function(){
        loginPage.visit();

        browser.manage().timeouts().implicitlyWait(2000);

        loginPage.submitButton.click();

        //expect(loginPage.erroLogin.isDisplayed().toBe(true));

    });
    it ('tentar logar na aplicacao sem informar senha teste', function(){
        loginPage.visit();

        loginPage.emailField.sendKeys('arthur@hotmail.com');
        browser.manage().timeouts().implicitlyWait(2000);

        loginPage.submitButton.click();

        //expect(loginPage.erroLogin.isDisplayed().toBe(true));

    });

    it('tentar logar na aplicacao sem informar o email teste', function(){
        loginPage.visit();

        browser.manage().timeouts().implicitlyWait(2000);

        loginPage.passwordField.sendKeys('123');
        browser.manage().timeouts().implicitlyWait(2000);
        loginPage.submitButton.click();

        //expect(loginPage.erroLogin.isDisplayed().toBe(true));


    });

    it('logar na aplicacao teste', function(){
        loginPage.visit();

        browser.manage().timeouts().implicitlyWait(2000);

        loginPage.fillForm('arthur@hotmail.com', '123');
        loginPage.submitButton.click();

    //    expect(homePage.sucessMessage.isDisplayed().toBe(true));
    });

});