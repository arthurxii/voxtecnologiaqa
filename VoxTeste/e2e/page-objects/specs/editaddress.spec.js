//editaddress.spec.js
'use strict'
var EditaddressPage = require('../editaddressPage.po.js');

describe('edit page', function(){
    var editaddressPage = new EditaddressPage();

    it('editar dados do endereco teste', function(){
        editaddressPage.visit();

        editaddressPage.addressUmFieldSaved.clear();
        editaddressPage.addressUmFieldSaved.sendKeys('Street of Dreams');

        editaddressPage.cityFieldSaved.clear();
        editaddressPage.cityFieldSaved.sendKeys('City of Angels');

        editaddressPage.zipCodeFieldSaved.clear();
        editaddressPage.zipCodeFieldSaved.sendKeys('58062180');

        editaddressPage.ageFieldSaved.clear();
        editaddressPage.ageFieldSaved.sendKeys('25');

        editaddressPage.updateButton.submit();

        //expect(addressSavedPage.sucessAddressMessage.isDisplayed()).toBe(true);

    });
});