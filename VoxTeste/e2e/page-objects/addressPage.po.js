//addressPage.po.js

var AddressPage = function(){
    this.tituloPaginaAddress = element(by.css(`//h2[contains(text(),'Addresses')]`));
    this.addressesButtonMenu = element(by.xpath(`//a[contains(text(),'Addresses')]`));
    this.newAddress = element(by.xpath(`//a[@class='row justify-content-center']`));
};

AddressPage.prototype.visit = function() {
    browser.get('#/address');
};

module.exports = AddressPage;