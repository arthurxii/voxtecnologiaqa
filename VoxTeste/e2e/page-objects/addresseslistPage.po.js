//addresseslistPage.po.js

var AddresseslistPage = function(){
    this.newAdressButton = element(by.xpath(`//a[@class='row justify-content-center']`));
    this.showButton = element(by.xpath(`//tr[1]//td[5]//a[1]`));
    this.editButton = element(by.xpath('//tr[1]//td[6]//a[1]'));
    this.destroyButton = element(by.xpath(`//tr[2]//td[7]//a[1]`));
    this.addressesButton = element(by.xpath(`//a[contains(text(),'Addresses')]`));
    this.destroyConfirmation = element(by.xpath('/html/body/div/table/tbody/tr/td[7]/a'));
};

AddresseslistPage.prototype.visit = function(){
    browser.get('/addresses');
};

module.exports = AddresseslistPage;