//editaddressPage.po.js

var EditaddressPage = function(){
    //this.tituloEditingAddress = element(by.css('h2'));

    this.firstNameFieldSaved = element(by.id('address_first_name'));
    this.lastNameFieldSaved = element(by.id('address_last_name'));
    this.addressUmFieldSaved = element(by.xpath(`//input[@id='address_street_address']`));
    this.addressDoisFieldSaved = element(by.id('address_secondary_address'));
    this.cityFieldSaved = element(by.id('address_city'));
    this.stateDropDownSaved = element(by.xpath(`//select[@id='address_state']`));
    //this.stateDropDownSaved = element(by.css("#address_state [value = 'AZ']"));
    this.zipCodeFieldSaved = element(by.id('address_zip_code'));
    this.countryRadioUSSaved = element(by.id('address_country_us'));
    this.countryRadioCNSaved = element(by.id('address_country_canada'));
    this.dateFieldSaved = element(by.id('address_birthday'));
    this.colorInputSaved = element(by.id('address_color'));
    this.ageFieldSaved = element(by.id('address_age'));
    this.websiteFieldSaved = element(by.id('address_website'));
    this.pictureFileSaved = element(by.id('address_picture'));
    this.phoneFieldSaved = element(by.id('address_phone'));
    this.commonCheckBoxClimbingSaved = element(by.id('address_interest_climb'));
    this.commonCheckBoxDancingSaved = element(by.id('address_interest_dance'));
    this.commonCheckBoxReadingSaved = element(by.id('address_interest_read'));
    this.noteFieldSaved = element(by.id('address_note'));

    this.updateButton = element(by.xpath(`//input[@name='commit']`));
    this.showButtonSaved = element(by.xpath(`//a[contains(text(),'Show')]`));
    this.listButtonSaved = element(by.xpath(`//a[contains(text(),'List')]`));
};

EditaddressPage.prototype.visit = function() {
    browser.get('/addresses/');
};

module.exports = EditaddressPage;