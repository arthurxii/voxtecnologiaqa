// protractor.conf.js
'use strict'
var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
var SpecReporter = require('jasmine-spec-reporter').SpecReporter;
var fs = require('fs');

module.exports.config = {
  framework: 'jasmine2',
  seleniumAddress: "http://localhost:4444/wd/hub",
  directConnect: true,
  capabilities: {
    browserName: "chrome"
  },
  //specs: ['/*.spec.js'],
  baseUrl: "http://a.testaddressbook.com/sign_in",
  suites: {
    login: './page-objects/specs/loginPage.spec.js',
    homepage: './page-objects/specs/homepage.spec.js',
    addressPage: './page-objects/specs/address.spec.js',
    newAddress: './page-objects/specs/newaddress.spec.js',
    addressSaved: './page-objects/specs/addressSaved.spec.js',
    addressList: './page-objects/specs/addresseslistpage.spec.js',
    editAddress: './page-objects/specs/editaddress.spec.js',
    destroyAddress: './page-objects/specs/destroyaddress.spec.js'
  },
  
    onPrepare(){
    jasmine.getEnv().addReporter(new SpecReporter({
      displayFailuresSummary: true,
      displayFailuredSpec: true,
      displaySuiteNumber: true,
      displaySpecDuration: true
    }));

    jasmine.getEnv().addReporter(new Jasmine2HtmlReporter({
      takeScreenshots: true,
      fixedScreenshotName: true
    }));
  }
  

};
